import os
from dotenv import load_dotenv
from app.controller import QRCodeScannerController


if __name__ == '__main__':
    load_dotenv()
    base_url = os.getenv('CAMERA_BASE_URL')
    username = os.getenv('CAMERA_USERNAME')
    password = os.getenv('CAMERA_PASSWORD')

    controller = QRCodeScannerController(
        base_url,
        username,
        password
    )

    controller.scan()

