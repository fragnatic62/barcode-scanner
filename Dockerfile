# Use an official Python runtime as a parent image
FROM python:3.11.4-slim-bullseye

# Turns off buffering for easier container logging
ENV PYTHONDONTWRITEBYTECODE=1

# Set the working directory in the container
WORKDIR /app

# Copy the current directory contents into the container at /app
COPY . /app

# Install required Python packages
RUN pip install --no-cache-dir -r requirements.txt

# Set the environment variable for Python to run in unbuffered mode (recommended for Docker)
ENV PYTHONUNBUFFERED 1

# Command to run the application
CMD ["python", "main.py"]

# docker build -t barcode-scanner-app .
# docker build -t barcode-scanner-app .