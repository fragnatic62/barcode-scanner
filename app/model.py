from pydantic import BaseModel
from typing import Set


class QRCodeScannerModel(BaseModel):
    processed_qr_codes: Set[str] = set()

