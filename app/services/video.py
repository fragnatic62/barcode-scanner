import cv2


class VideoStreamService:
    def __init__(self, base_url, username, password, frame_rate):
        self.rtsp_url = f'rtsp://{username}:{password}@{base_url}:554/cam/realmonitor?channel=1&subtype=1'
        self.frame_rate = frame_rate
        self.cap = cv2.VideoCapture(self.rtsp_url)

    def set_frame_rate(self):
        self.cap.set(cv2.CAP_PROP_FPS, self.frame_rate)

    def read_frames(self, stop_threads, frame_callback):
        self.set_frame_rate()
        while not stop_threads:
            ret, frame = self.cap.read()
            if not ret:
                break
            frame_callback(frame)

    def release(self):
        self.cap.release()