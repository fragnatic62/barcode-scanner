import requests


class DahuaCameraService:
    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.username = username
        self.password = password

    def set_custom_title_text(self, text):
        url = f'http://{self.base_url}/cgi-bin/configManager.cgi?action=setConfig&VideoWidget[0].CustomTitle[1].Text={text}'
        return self._send_request(url)

    def _send_request(self, url):
        try:
            auth = requests.auth.HTTPDigestAuth(self.username, self.password)
            response = requests.get(url, auth=auth, timeout=5)
            response.raise_for_status()
            return response.text
        except requests.exceptions.Timeout as exception:
            print("TimeoutError fetching information from", url)
            raise exception
        except requests.exceptions.RequestException as exception:
            print("Request failed fetching information from", url)
            raise exception

