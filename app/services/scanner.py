import cv2
from pyzbar.pyzbar import decode
from typing import Callable
from app.model import QRCodeScannerModel
from pydantic import parse_obj_as


class QRCodeScannerService:
    def __init__(self, model: QRCodeScannerModel, scan_callback: Callable[[str], None]):
        self.model = model
        self.scan_callback = scan_callback

    def scan_barcodes(self, frame):
        barcodes = decode(frame)

        for barcode in barcodes:
            qr_code_data = barcode.data.decode('utf-8')

            if qr_code_data and qr_code_data not in self.model.processed_qr_codes:
                print('QR Code data:', qr_code_data)
                self.model.processed_qr_codes.add(qr_code_data)
                self.scan_callback(qr_code_data)