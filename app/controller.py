import threading
import time
from pyzbar.pyzbar import decode
from app.services.scanner import QRCodeScannerService
from app.services.camera import DahuaCameraService
from app.model import QRCodeScannerModel
from pydantic import parse_obj_as
from app.services.video import VideoStreamService


class QRCodeScannerController:
    def __init__(self, base_url, username, password):
        self.base_url = base_url
        self.username = username
        self.password = password
        self.model = QRCodeScannerModel()

        # Services
        self.qr_scanner_service = QRCodeScannerService(self.model, self.handle_scan)
        self.video_stream_service = VideoStreamService(
            base_url=self.base_url,
            username=self.username,
            password=self.password,
            frame_rate=20
        )
        self.camera_custom_components_service = DahuaCameraService(
            base_url=self.base_url,
            username=self.username,
            password=self.password
        )

    def handle_scan(self, qr_code_data):
        # Add your scan handling logic here
        self.camera_custom_components_service.set_custom_title_text('QRCODE: '+ qr_code_data)
        time.sleep(1)
        self.camera_custom_components_service.set_custom_title_text('Waiting QR to Scan...')

    def frame_callback(self, frame):
        self.qr_scanner_service.scan_barcodes(frame)

    def scan(self):
        stop_threads = False

        frame_thread = threading.Thread(target=self.video_stream_service.read_frames, args=(stop_threads, self.frame_callback))
        frame_thread.start()

        input("Press Enter to stop...")

        stop_threads = True
        frame_thread.join()
        video_stream_service.release()
